/**
 * @example    rc_test_quaternion_slerp.c
 *
 * @brief      Tests the slerp function in rc_quaternion.h
 *
 * @author     James Strawson
 * @date       1/29/2018
 */

#include <stdio.h>
#include <rc_math.h>


int main()
{
    double t, roll, pitch, yaw;
    rc_vector_t tb0 = RC_VECTOR_INITIALIZER;
    rc_vector_t tb1 = RC_VECTOR_INITIALIZER;

    rc_vector_t q0 = RC_VECTOR_INITIALIZER;
    rc_vector_t q1 = RC_VECTOR_INITIALIZER;
    rc_vector_t neg_q1 = RC_VECTOR_INITIALIZER;
    rc_vector_t q2 = RC_VECTOR_INITIALIZER;
    rc_matrix_t R  = RC_MATRIX_INITIALIZER;

    //set roll, pitch and yaw angles to interpolate between
    // double rpy0[] = {0.1, 0.2, 3.0};
    // double rpy1[] = {0.11, 0.21, 3.1};

    double rpy0[] = {0.0, 0.0, 0.0};
    double rpy1[] = {0.2, 0.0, 0.0};

    printf("RPY0: %f, %f, %f\n", rpy0[0], rpy0[1], rpy0[2]);
    printf("RPY1: %f, %f, %f\n", rpy1[0], rpy1[1], rpy1[2]);

    rc_rotation_matrix_from_tait_bryan(rpy0[0], rpy0[1], rpy0[2], &R);
    rc_rotation_to_quaternion(R, &q0);

    rc_rotation_matrix_from_tait_bryan(rpy1[0], rpy1[1], rpy1[2], &R);
    rc_rotation_to_quaternion(R, &q1);

    // printf("q0");
    // rc_vector_print(q0);

    // printf("q1");
    // rc_vector_print(q1);

    printf("\ninterpolate between q0 and q1\n");

    for (int i = 0; i < 11; i++)
    {
        t = i*0.1;
        rc_quaternion_slerp(q0, q1, t, &q2);
        rc_quaternion_to_rotation_matrix(q2, &R);
        rc_rotation_to_tait_bryan(R, &roll, &pitch, &yaw);
        printf("t=%0.2f: ",t);
        printf("Roll: %0.3f Pitch %0.3f Yaw: %0.3f\n", roll, pitch, yaw);
        // rc_vector_print(q2);
    }

    printf("\ninterpolate between q0 and -q1\n");
    neg_q1 = q1;
    neg_q1.d[0] = -q1.d[0];
    neg_q1.d[1] = -q1.d[1];
    neg_q1.d[2] = -q1.d[2];
    neg_q1.d[3] = -q1.d[3];

    for (int i = 0; i < 11; i++)
    {
        t = i*0.1;
        rc_quaternion_slerp(q0, neg_q1, t, &q2);
        rc_quaternion_to_rotation_matrix(q2, &R);
        rc_rotation_to_tait_bryan(R, &roll, &pitch, &yaw);
        printf("t=%0.2f: ",t);
        printf("Roll: %0.3f Pitch %0.3f Yaw: %0.3f\n", roll, pitch, yaw);
        // rc_vector_print(q2);
    }

    rc_vector_free(&tb0);
    rc_vector_free(&tb1);
    rc_vector_free(&q0);
    rc_vector_free(&q1);
    rc_vector_free(&q2);
    rc_matrix_free(&R);

    printf("\nDONE\n");
    return 0;
}
